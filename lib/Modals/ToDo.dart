class ToDo {
  String? id;
  String? todoText;
  bool isDone;

  ToDo({required this.id, required this.todoText, this.isDone = false});

  static List<ToDo> todoList() {
    return [
      ToDo(id: '01', todoText: 'Check mail', isDone: true),
      ToDo(id: '02', todoText: 'Groceries'),
      ToDo(id: '03', todoText: 'Homework'),
      ToDo(id: '04', todoText: 'Chores')
    ];
  }

  static List<ToDo> baseTodoList() {
    return [
      ToDo(id: '01', todoText: 'Welcome to your todo list'),
      ToDo(id: '02', todoText: 'Tap to check your task', isDone: true),
      ToDo(id: '03', todoText: 'Long press to edit your task'),
    ];
  }
}
