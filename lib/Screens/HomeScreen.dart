// import 'package:firebase_database/firebase_database.dart';
import 'dart:ui';


import 'package:flutter/material.dart';
import 'package:flutter/services.dart';

import '../Modals/ToDo.dart';
import '../Widgets/ToDoItem.dart';
import '../Widgets/NewItemDialog.dart';

import '../Widgets/editItemDilaog.dart';

class HomeScreen extends StatefulWidget {
  HomeScreen({super.key});

  @override
  State<HomeScreen> createState() => _HomeScreenState();
}

class _HomeScreenState extends State<HomeScreen> {
  final todoList = ToDo.baseTodoList();
  final _todoController = TextEditingController();

  // final List<ToDo> dbtodoList = ToDo.baseTodoList();
  // // final FB = FirebaseDatabase.instance;
  // late DatabaseReference DB;

  // @override
  // void initState() {
  //   super.initState();
  //   DB = FirebaseDatabase.instance.ref().child('TodoList');
  // }

  // _createDBTodo(){
  //   documentref
  // }
  // _deleteDBTodo(){

  // }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        appBar: AppBar(
          title: const Center(child: Text("Todo List")),
        ),
        body: Stack(
          children: [
            Expanded(
              child: ListView(
                children: [
                  Container(
                    margin: const EdgeInsets.only(top: 50, bottom: 20),
                    child: const Text(
                      "All Todos",
                      style:
                          TextStyle(fontSize: 30, fontWeight: FontWeight.bold),
                    ),
                  ),
                  for (ToDo todo in todoList)
                    ToDoItem(
                      todo: todo,
                      onTodoChange: _handleTodoChange,
                      onDeleteItem: _deleteTodoItem,
                      onLongPress: _handleLongPress,
                    ),
                ],
              ),
            ),
            Align(
              alignment: Alignment.bottomCenter,
              child: Row(
                children: [
                  Expanded(
                      child: Container(
                    margin:
                        const EdgeInsets.only(bottom: 20, right: 20, left: 20),
                    padding:
                        const EdgeInsets.symmetric(horizontal: 20, vertical: 5),
                    decoration: BoxDecoration(
                        color: Colors.white,
                        boxShadow: const [
                          BoxShadow(
                              color: Colors.blueGrey,
                              offset: Offset(0.0, 0.0),
                              blurRadius: 10.0,
                              spreadRadius: 0.0),
                        ],
                        borderRadius: BorderRadius.circular(10)),
                    child: TextField(
                      controller: _todoController,
                      decoration: const InputDecoration(
                          hintText: 'Add a new todo item',
                          border: InputBorder.none),
                    ),
                  )),
                  Container(
                      margin: const EdgeInsets.only(bottom: 20, right: 20),
                      child: FloatingActionButton(
                        onPressed: () {
                          _todoController.text.isEmpty
                              ? showDialog<void>(
                                  context: context,
                                  builder: (BuildContext context) {
                                    return NewItemDialog(
                                      onSave: _addTodoItem,
                                    );
                                  })
                              : _addTodoItem(_todoController.text);
                        },
                        shape: RoundedRectangleBorder(
                            borderRadius: BorderRadius.circular(10)),
                        backgroundColor: Colors.red,
                        child: const Icon(Icons.add),
                      ))
                ],
              ),
            ),
          ],
        ));
  }

  void _handleTodoChange(ToDo todo) {
    setState(() {
      todo.isDone = !todo.isDone;
    });
  }

  void _deleteTodoItem(String id) {
    setState(() {
      todoList.removeWhere((item) => item.id == id);
    });
  }

  void _addTodoItem(String text) {
    setState(() {
      todoList.add(ToDo(
          id: DateTime.now().millisecondsSinceEpoch.toString(),
          todoText: text));
    });
    _todoController.clear();
  }

  void _editTask(String oldText, String newText) {
    setState(() {
      todoList[todoList.indexWhere((element) => element.todoText == oldText)]
          .todoText = newText;
    });
  }

  _handleLongPress(String rootText) {
    showDialog(
        context: context,
        builder: (BuildContext context) {
          return EditItemDialog(
            rootText: rootText,
            onSave: _editTask,
          );
        });
  }

  // void _addDBTodoItem(String toDo) {
  //   setState(() {
  //     DB.push().set(ToDo(
  //         id: DateTime.now().millisecondsSinceEpoch.toString(),
  //         todoText: toDo));
  //   });
  // }
}

// Future<void> _showNewItemDialog(List<ToDo> rootList) {

//   return showDialog<void>(
//       context: context,
//       builder: (BuildContext context) {
//         return  NewItemDialog(
//           rootList: rootList,
//         );
//       });
// }
