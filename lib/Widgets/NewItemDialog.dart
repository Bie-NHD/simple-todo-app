import 'dart:ui';

import 'package:flutter/foundation.dart';
import 'package:flutter/material.dart';
import 'package:simple_todo_app/Modals/ToDo.dart';

class NewItemDialog extends StatelessWidget {
  final newItemDialogTextController = TextEditingController();

  final onSave;

  NewItemDialog({Key? key, required this.onSave}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return BackdropFilter(
        filter: ImageFilter.blur(sigmaX: 1.5, sigmaY: 1.5),
        child: Dialog(
          insetPadding: EdgeInsets.all(20),
          shape:
              RoundedRectangleBorder(borderRadius: BorderRadius.circular(15.0)),
          backgroundColor: Colors.white,
          child: Container(
            width: 200,
            height: 240,
            margin: EdgeInsets.symmetric(horizontal: 5, vertical: 10),
            padding: EdgeInsets.symmetric(horizontal: 15),
            child: Column(
              mainAxisAlignment: MainAxisAlignment.spaceEvenly,
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                Row(
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  children: [
                    Text(
                      'New Todo Item',
                      style:
                          TextStyle(fontSize: 20, fontWeight: FontWeight.bold),
                    ),
                    Align(
                      alignment: Alignment.topRight,
                      child: IconButton(
                          iconSize: 30,
                          onPressed: () {
                            // Navigator.pop(newItemDialogContext);
                            Navigator.of(context).pop();
                          },
                          icon: const Icon(Icons.close)),
                    ),
                  ],
                ),
                Container(
                  height: 80,
                  child: TextField(
                    controller: newItemDialogTextController,
                    expands: true,
                    maxLines: null,
                    decoration: const InputDecoration(
                        contentPadding: EdgeInsets.all(10),
                        hintText: 'Add a new todo item',
                        border: OutlineInputBorder()),
                  ),
                ),
                Align(
                  alignment: Alignment.bottomRight,
                  child: Container(
                    height: 55,
                    width: 55,
                    decoration: BoxDecoration(
                        borderRadius: BorderRadius.circular(90),
                        color: Colors.red,
                        boxShadow: [
                          BoxShadow(
                              color: Colors.grey.shade400.withOpacity(0.8),
                              spreadRadius: 3,
                              blurRadius: 8,
                              offset: const Offset(3, 5))
                        ]),
                    child: IconButton(
                      onPressed: () {
                        newItemDialogTextController.text.isNotEmpty
                            ? onSave(newItemDialogTextController.text)
                            : null;
                        Navigator.of(context).pop();

                        // Navigator.pop(newItemDialogContext);
                      },
                      icon: const Icon(
                        Icons.add,
                        color: Colors.white,
                      ),
                    ),
                  ),
                ),
              ],
            ),
          ),
        ));
  }
}
