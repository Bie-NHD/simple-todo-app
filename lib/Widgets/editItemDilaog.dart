import 'dart:ui';

import 'package:flutter/material.dart';

class EditItemDialog extends StatelessWidget {
  final String rootText;
  final onSave;
  final _controller = TextEditingController();

  EditItemDialog({Key? key, required this.rootText, required this.onSave})
      : super(key: key);

  @override
  Widget build(BuildContext context) {
    return BackdropFilter(
      filter: ImageFilter.blur(sigmaX: 5, sigmaY: 5),
      child: AlertDialog(
        shape: RoundedRectangleBorder(borderRadius: BorderRadius.circular(5)),
        backgroundColor: Colors.white,
        title:
            Row(mainAxisAlignment: MainAxisAlignment.spaceBetween, children: [
          Text(
            'Edit your task',
            style: TextStyle(fontSize: 20, fontWeight: FontWeight.bold),
          ),
          IconButton(
              onPressed: () {
                Navigator.of(context).pop();
              },
              icon: Icon(Icons.close))
        ]),
        content: Container(
          height: 100,
          child: Column(
            mainAxisAlignment: MainAxisAlignment.center,
            children: [
              Container(
                height: 50,
                width: MediaQuery.of(context).size.width,
                decoration: BoxDecoration(
                    borderRadius: BorderRadius.circular(10),
                    color: Colors.white,
                    boxShadow: [
                      BoxShadow(
                          color: Colors.blueGrey.shade100,
                          offset: Offset(3, 3),
                          blurRadius: 10,
                          spreadRadius: 0),
                    ]),
                child: TextField(
                  controller: _controller,
                  decoration: InputDecoration(
                      contentPadding: EdgeInsets.all(5),
                      hintText: rootText,
                      border: InputBorder.none,
                      fillColor: Colors.grey[300]),
                ),
              ),
              Row(
                mainAxisAlignment: MainAxisAlignment.end,
                children: [
                  TextButton(
                      onPressed: () {
                        Navigator.of(context).pop();
                      },
                      child: Text(
                        'Cancel',
                        style: TextStyle(
                            color: Colors.blue,
                            fontSize: 15,
                            fontWeight: FontWeight.bold),
                      )),
                  TextButton(
                      onPressed: () {
                        _controller.text.isEmpty
                            ? null
                            : onSave(rootText, _controller.text);
                        Navigator.of(context).pop();
                      },
                      child: Text('Save',
                          style: TextStyle(
                              color: Colors.blue,
                              fontSize: 15,
                              fontWeight: FontWeight.bold)))
                ],
              )
            ],
          ),
        ),
      ),
    );
  }
}
