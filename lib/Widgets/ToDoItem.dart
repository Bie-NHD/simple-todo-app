import 'package:flutter/material.dart';
import 'package:simple_todo_app/Modals/ToDo.dart';

class ToDoItem extends StatelessWidget {
  final ToDo todo;
  final onTodoChange;
  final onDeleteItem;
  final onLongPress;

  const ToDoItem(
      {Key? key,
      required this.todo,
      required this.onTodoChange,
      required this.onDeleteItem,required this.onLongPress})
      : super(key: key);

  @override
  Widget build(BuildContext context) {
    // TODO: implement build
    return Container(
        margin: EdgeInsets.only(bottom: 20),
        child: ListTile(
          onTap: () {
            onTodoChange(todo);
          },
          onLongPress: () {
            onLongPress(todo.todoText);
          },
          shape:
              RoundedRectangleBorder(borderRadius: BorderRadius.circular(20)),
          contentPadding: EdgeInsets.symmetric(horizontal: 20, vertical: 5),
          tileColor: Colors.white,
          leading: Icon(
              todo.isDone ? Icons.check_box : Icons.check_box_outline_blank,
              color: Colors.black),
          title: Text(todo.todoText!,
              style: TextStyle(
                  color: todo.isDone ? Colors.grey : Colors.black,
                  decoration: todo.isDone ? TextDecoration.lineThrough : null)),
          trailing: Container(
            padding: EdgeInsets.all(0),
            margin: EdgeInsets.symmetric(vertical: 12),
            height: 35,
            width: 35,
            decoration: BoxDecoration(
                color: Colors.red, borderRadius: BorderRadius.circular(5)),
            child: IconButton(
              icon: Icon(Icons.delete),
              color: Colors.white,
              iconSize: 18,
              onPressed: () {
                onDeleteItem(todo.id);
              },
            ),
          ),
        ));
  }
}
